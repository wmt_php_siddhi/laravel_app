<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
   public function dashboard(){
       return view('dashboard');
  }
    public function postSignUp(Request $request)
    {

        $request->validate([

            'email' => ['required', 'string', 'email', 'max:255'],
            'username' => ['required','string','min:3','max:30','alpha'],
//            'mobile'=>['required','regex:/^([0-9\s\-\+\(\)]*)$/','digits:10','numeric'],
            'password' => ['required', 'alpha_dash', 'min:8'],
        ]);


        $user = new User();
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = $request->password;

        $user->save();

        return view('dashboard');

    }
    public function postSignIn(Request $request)
    {


        if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']])){
            return view('dashboard');
        }

            return view('welcome');
    }

}
